import static org.junit.Assert.*;

import java.net.HttpURLConnection;

import org.junit.Test;

public class createExperiment {
	
	@Test
	public void test() {
		String destination = "http://127.0.0.1:8888/api/createExperiment";
		//String newExperiment = "%7B%22admins%22%3A%5B%22ema.hanze%40gmail.com%22%5D%2C+%22creator%22%3A%22ema.hanze%40gmail.com%22%2C+%22contactEmail%22%3A%22ema.hanze%40gmail.com%22%2C+%22extraDataCollectionDeclarations%22%3A%5B%5D%2C+%22groups%22%3A%5B%7B%22actionTriggers%22%3A%5B%5D%2C+++++%22name%22%3A%22New+Group%22%2C+++++%22inputs%22%3A%5B%5D%2C+++++%22feedbackType%22%3A0%2C+++++%22feedback%22%3A%7B%22type%22%3A0%2C%22text%22%3A%22Thanks+for+Participating!%22%7D%2C+++++%22fixedDuration%22%3A%22false%22%7D%5D%2C+%22postInstallInstructions%22%3A%22%3Cb%3EYou+have+successfully+joined+the+experiment!%3C%2Fb%3E%3Cbr%2F%3E%3Cbr%2F%3E%5CnNo+need+to+do+anything+else+for+now.%3Cbr%2F%3E%3Cbr%2F%3E%5CnPaco+will+send+you+a+notification+when+it+is+time+to+participate.%3Cbr%2F%3E%3Cbr%2F%3E%5CnBe+sure+your+ringer%2Fbuzzer+is+on+so+you+will+hear+the+notification.%22%2C+%22published%22%3Afalse%2C+%22publishedUsers%22%3A%5B%22testUser%40test.com%22%5D%2C+%22ringtoneUri%22%3A%22%2Fassets%2Fringtone%2FPaco+Bark%22%2C+%22title%22%3A%22FullJsonPost%22%2C+%22organization%22%3A%22organization%22%2C+%22description%22%3A%22description%22%7D";
		String newExperiment = "{\"title\":\"TitleEdited\",\"description\":\"description\",\"creator\":\"test@example.com\",\"organization\":\"organization\",\"contactEmail\":\"test@example.com\",\"id\":5207287069147136,\"recordPhoneDetails\":false,\"extraDataCollectionDeclarations\":[],\"deleted\":false,\"modifyDate\":\"2016/11/04\",\"published\":false,\"admins\":[\"test@example.com\"],\"publishedUsers\":[\"gin.kevin.92@gmail.com\"],\"version\":2,\"groups\":[{\"name\":\"NewGroup\",\"customRendering\":false,\"fixedDuration\":false,\"logActions\":false,\"logShutdown\":false,\"backgroundListen\":false,\"accessibilityListen\":false,\"actionTriggers\":[],\"inputs\":[],\"endOfDayGroup\":false,\"feedback\":{\"text\":\"Thanks for Participating!\",\"type\":0,\"nameOfClass\":\"com.pacoapp.paco.shared.model2.Feedback\"},\"feedbackType\":0,\"nameOfClass\":\"com.pacoapp.paco.shared.model2.ExperimentGroup\"}],\"ringtoneUri\":\"/assets/ringtone/Paco Bark\",\"postInstallInstructions\":\"<b>You have successfully joined the experiment!</b><br/><br/>\nNo need to do anything else for now.<br/><br/>\nPaco will send you a notification when it is time to participate.<br/><br/>\nBe sure your ringer/buzzer is on so you will hear the notification.\",\"nameOfClass\":\"com.pacoapp.paco.shared.model2.ExperimentDAO\"}";
		HttpURLConnection connection = outboundTraffic.Create("json=" + newExperiment, destination); 
		if(connection != null){
			if(!inboundTraffic.getResponse(connection)) fail("experiment could not be created");
		}
		else fail("connection not established");
	}
	
}
