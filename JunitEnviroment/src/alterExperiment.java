import static org.junit.Assert.*;

import java.net.HttpURLConnection;

import org.junit.Test;

public class alterExperiment {

	@Test
	public void test() {
		String destination = "http://localhost:1244/api/alterExperiment";
		String modifiedExperiment = "%7B%22admins%22%3A%5B%22ema.hanze%40gmail.com%22%5D%2C+%22creator%22%3A%22ema.hanze%40gmail.com%22%2C+%22contactEmail%22%3A%22ema.hanze%40gmail.com%22%2C+%22extraDataCollectionDeclarations%22%3A%5B%5D%2C+%22groups%22%3A%5B%7B%22actionTriggers%22%3A%5B%5D%2C+++++%22name%22%3A%22New+Group%22%2C+++++%22inputs%22%3A%5B%5D%2C+++++%22feedbackType%22%3A0%2C+++++%22feedback%22%3A%7B%22type%22%3A0%2C%22text%22%3A%22Thanks+for+Participating!%22%7D%2C+++++%22fixedDuration%22%3A%22false%22%7D%5D%2C+%22postInstallInstructions%22%3A%22%3Cb%3EYou+bitch+please+have+successfully+joined+the+experiment!%3C%2Fb%3E%3Cbr%2F%3E%3Cbr%2F%3E%5CnNo+need+to+do+anything+else+for+now.%3Cbr%2F%3E%3Cbr%2F%3E%5CnPaco+will+send+you+a+notification+when+it+is+time+to+participate.%3Cbr%2F%3E%3Cbr%2F%3E%5CnBe+sure+your+ringer%2Fbuzzer+is+on+so+you+will+hear+the+notification.%22%2C+%22published%22%3Afalse%2C+%22publishedUsers%22%3A%5B%22testUser%40test.com%22%5D%2C+%22ringtoneUri%22%3A%22%2Fassets%2Fringtone%2FPaco+Bark%22%2C+%22title%22%3A%22FullJsonPost%22%2C+%22organization%22%3A%22organization%22%2C+%22description%22%3A%22description%22%7D";
		String deleteExperiment = "5629499534213120"; //change to desired experiment manually. use createExperiment output

		HttpURLConnection connection = outboundTraffic.Create("id=" + deleteExperiment + "&json=" + modifiedExperiment, destination); 
		if(connection != null){
			if(!inboundTraffic.getResponse(connection)) fail("experiment could not be found or altered");
		}
		else fail("connection not established");
	}
	

}
