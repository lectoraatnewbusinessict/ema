import static org.junit.Assert.*;

import java.net.HttpURLConnection;

import org.junit.Test;

public class getExperiment {

	@Test
	public void test() {
		String destination = "http://127.0.0.1:8888/api/getExperiment";
		String getExperiment = "6544293208522752"; //change to desired experiment manually. use createExperiment output
		HttpURLConnection connection = outboundTraffic.Create("id=" + getExperiment, destination); 
		if(connection != null){
			if(!inboundTraffic.getResponse(connection)) fail("no experiment with this ID");
		}
		else fail("connection not established");
	}


}
