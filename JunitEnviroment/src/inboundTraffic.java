import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;

/**
 * Class to help Unit test the server environment
 * Use to print the API responds
 */
public class inboundTraffic {
	
	private inboundTraffic(){
		
	}
	
	public static boolean getResponse(HttpURLConnection output){
		Reader in;
		try {
			in = new BufferedReader(new InputStreamReader(output.getInputStream(), "UTF-8"));
			for (int c; (c = in.read()) >= 0;) {
	            System.out.print((char)c);
	        }
		} 
		catch (IOException e) {
			return false;
		}
		return true;
	}
}
