import static org.junit.Assert.*;

import java.net.HttpURLConnection;

import org.junit.Test;

public class deleteExperiment {

	@Test
	public void test() {
		String destination = "http://127.0.0.1:8888/api/deleteExperiment";
		String deleteExperiment = "6544293208522752"; //change to desired experiment manually. use createExperiment output
		HttpURLConnection connection = outboundTraffic.Create("id=" + deleteExperiment, destination); 
		if(connection != null){
			if(!inboundTraffic.getResponse(connection)) fail("experiment could not be deleted");
		}
		else fail("connection not established");
	}
}
