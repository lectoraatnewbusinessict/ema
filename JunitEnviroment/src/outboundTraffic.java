import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Class to help Unit test the server environment
 * Use to call the API with parameters
 */
public class outboundTraffic {
	
	private outboundTraffic(){
		
	}
	
	/**
	 * Static method to send outbound POST traffic to the paco server
	 * @param parameters to send to the server
	 * @param targetUrl  the server API address
	 * @return the HttpURLConnection to the paco API
	 */
	public static HttpURLConnection Create(String parameters, String targetUrl){
		 byte[] postData       = parameters.getBytes(StandardCharsets.UTF_8);        
	     HttpURLConnection conn;
		 try {
			conn = (HttpURLConnection) new URL(targetUrl).openConnection();
			conn.setDoOutput( true );
		    conn.setInstanceFollowRedirects( false );
		    conn.setRequestMethod("POST");
		    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
		    conn.setRequestProperty("charset", "utf-8");
		    conn.setRequestProperty("Content-Length", Integer.toString(postData.length));
		    conn.setUseCaches(false);
		    DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
		    wr.write(postData);
		 }
		 catch (IOException e) {
			return null;
		 }
		return conn;
	}
}
