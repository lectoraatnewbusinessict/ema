package api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTimeZone;

import com.google.common.collect.Lists;
import com.google.sampling.experiential.server.ExperimentJsonUploadProcessor;
import com.google.sampling.experiential.server.ExperimentService;
import com.google.sampling.experiential.server.ExperimentServiceFactory;
import com.google.sampling.experiential.server.ExperimentServletSelectedExperimentsFullLoadHandler;
import com.google.sampling.experiential.server.TimeUtil;
import com.pacoapp.paco.shared.comm.Outcome;

/**
 * Class responsible for handling POST get experiment input
 */
public class getExperiment extends HttpServlet {
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json;charset=UTF-8");
		
		String selectedExperimentsParam = req.getParameter("id");
		DateTimeZone timezone = TimeUtil.getTimeZoneForClient(req);
		resp.getWriter().println(ExperimentJsonUploadProcessor.toJson(getExperiments(deleteExperiment.HiLabAuthentication,selectedExperimentsParam, timezone)));
   }
	
	public List<Outcome> createErrorOutcome(String msg) {
	    Outcome outcome = new Outcome(0, msg);
	    List<Outcome> outcomes = Lists.newArrayList(outcome);
	    return outcomes;
	}
	
	private List getExperiments(String email, String selectedExperimentsParam, DateTimeZone timezone) {
	    ExperimentService expService = ExperimentServiceFactory.getExperimentService();
	    List<Long> experimentIds = ExperimentServletSelectedExperimentsFullLoadHandler.parseExperimentIds(selectedExperimentsParam);
	    List getExperimentsResult = new ArrayList();
	    if (experimentIds.isEmpty()) {
	      return createErrorOutcome("No experiment id specified for deletion");
	    }
	    Outcome outcome = new Outcome();
	    List<Outcome> outcomeList = Lists.newArrayList();
	    outcomeList.add(outcome);
	    try {
	      getExperimentsResult = expService.getExperimentsById(experimentIds, email, timezone);
	      if (getExperimentsResult.isEmpty()){
	        outcome.setError("Could not get experiment. Rolled back.");
	        return outcomeList;
	      }
	    } catch (Exception e) {
	      outcome.setError("Could not get experiment. Rolled back. Error: " + e.getMessage());
	      return outcomeList;
	    }

	    return getExperimentsResult;
	  }
}
