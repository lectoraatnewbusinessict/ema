package api;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.DateTimeZone;

import com.google.appengine.api.users.User;
import com.google.sampling.experiential.server.AuthUtil;
import com.google.sampling.experiential.server.ExperimentJsonUploadProcessor;
import com.google.sampling.experiential.server.TimeUtil;

/**
 * Class responsible for handling POST create experiment input
 * expects post parameter json in HTML format
 * see JunitTest for format
 */
public class createExperiment extends HttpServlet {	
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json;charset=UTF-8");
				
		String postBodyString = req.getParameter("json");
		//String result = StringEscapeUtils.unescapeHtml4(postBodyString);//escapeHtml4(postBodyString);
		//System.out.println("JSON input parsed to HTML: " + result);
		
		DateTimeZone timezone = TimeUtil.getTimeZoneForClient(req);
		final User whoFromLogin = AuthUtil.getUserForAPI();//getWhoFromLogin();
		
		String results = ExperimentJsonUploadProcessor.create().processJsonExperiments(postBodyString, whoFromLogin, "local test header", "1.0", timezone);
	    resp.getWriter().write(results);
   }
}