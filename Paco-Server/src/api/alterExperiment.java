package api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTimeZone;

import com.google.common.collect.Lists;
import com.google.sampling.experiential.server.ExperimentJsonUploadProcessor;
import com.google.sampling.experiential.server.ExperimentService;
import com.google.sampling.experiential.server.ExperimentServiceFactory;
import com.google.sampling.experiential.server.ExperimentServletSelectedExperimentsFullLoadHandler;
import com.google.sampling.experiential.server.TimeUtil;
import com.pacoapp.paco.shared.comm.Outcome;
import com.pacoapp.paco.shared.model2.ExperimentDAO;
import com.pacoapp.paco.shared.model2.JsonConverter;

/**
 * Class responsible for handling POST modify experiment input
 */
public class alterExperiment extends HttpServlet {
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json;charset=UTF-8");
		
		String selectedExperimentsParam = req.getParameter("id");
		String postBodyString = req.getParameter("json");
		
		DateTimeZone timezone = TimeUtil.getTimeZoneForClient(req);

		resp.getWriter().println(ExperimentJsonUploadProcessor.toJson(modifyExperiments(deleteExperiment.HiLabAuthentication, timezone, selectedExperimentsParam, postBodyString)));
   }
	
	private List<Outcome> modifyExperiments(String email, DateTimeZone timezone, String selectedExperimentsParam, String newBody)
	{
	    ExperimentService expService = ExperimentServiceFactory.getExperimentService();
	    List<Long> experimentIds = ExperimentServletSelectedExperimentsFullLoadHandler.parseExperimentIds(selectedExperimentsParam);
	    if (experimentIds.isEmpty()) {
	      return createErrorOutcome("No experiment id specified for deletion");
	    }
	    if(experimentIds.size() > 1)
	    {
		      return createErrorOutcome("Can only modify one experiment at a time"); 
	    }
	    Outcome outcome = new Outcome();
	    List<Outcome> outcomeList = Lists.newArrayList();
	    outcomeList.add(outcome);

	    ExperimentDAO expirementToModify = expService.getExperiment(experimentIds.get(0));
	    	    
	    ExperimentDAO modifiedExperiment =  JsonConverter.fromSingleEntityJson(newBody);
	    
	    expirementToModify.setAdmins(modifiedExperiment.getAdmins());
	    expirementToModify.setContactEmail(modifiedExperiment.getContactEmail());
	    expirementToModify.setContactPhone(modifiedExperiment.getContactPhone());
	    expirementToModify.setCreator(modifiedExperiment.getCreator());
	    expirementToModify.setDeleted(modifiedExperiment.getDeleted());
	    expirementToModify.setDescription(modifiedExperiment.getDescription());
	    expirementToModify.setEarliestStartDate(modifiedExperiment.getEarliestStartDate());
	    expirementToModify.setExtraDataCollectionDeclarations(modifiedExperiment.getExtraDataCollectionDeclarations());
	    expirementToModify.setGroups(modifiedExperiment.getGroups());
	    expirementToModify.setInformedConsentForm(modifiedExperiment.getInformedConsentForm());
	    expirementToModify.setJoinDate(modifiedExperiment.getJoinDate());
	    expirementToModify.setLatestEndDate(modifiedExperiment.getLatestEndDate());
	    expirementToModify.setOrganization(modifiedExperiment.getOrganization());
	    expirementToModify.setPostInstallInstructions(modifiedExperiment.getPostInstallInstructions());
	    expirementToModify.setPublicKey(modifiedExperiment.getPublicKey());
	    expirementToModify.setPublished(modifiedExperiment.getPublished());
	    expirementToModify.setPublishedUsers(modifiedExperiment.getPublishedUsers());
	    expirementToModify.setRecordPhoneDetails(modifiedExperiment.getRecordPhoneDetails());
	    expirementToModify.setRingtoneUri(modifiedExperiment.getRingtoneUri());
	    expirementToModify.setTitle(modifiedExperiment.getTitle());
	    expirementToModify.setVersion(modifiedExperiment.getVersion());

	    expService.saveExperiment(expirementToModify, email, timezone);
	    
	    return outcomeList;
	}
		
	/**
	 * Create a list of error messages
	 * @param msg: the parsed result
	 * @return: a list of separate messages containing context to the error
	 */
	public List<Outcome> createErrorOutcome(String msg) {
	    Outcome outcome = new Outcome(0, msg);
	    List<Outcome> outcomes = Lists.newArrayList(outcome);
	    return outcomes;
	  }
	
}
