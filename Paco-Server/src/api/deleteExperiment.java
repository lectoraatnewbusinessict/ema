package api;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.common.collect.Lists;
import com.google.sampling.experiential.server.AuthUtil;
import com.google.sampling.experiential.server.ExperimentJsonUploadProcessor;
import com.google.sampling.experiential.server.ExperimentService;
import com.google.sampling.experiential.server.ExperimentServiceFactory;
import com.google.sampling.experiential.server.ExperimentServletSelectedExperimentsFullLoadHandler;
import com.pacoapp.paco.shared.comm.Outcome;

/**
 * Class responsible for handling POST delete experiment input
 * expects post parameter json
 * see JunitTest for format
 */
public class deleteExperiment extends HttpServlet {	
    static final String HiLabAuthentication = "ema.hanze@gmail.com";
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("application/json;charset=UTF-8");
		
		String selectedExperimentsParam = req.getParameter("id");
		
		bypass(HiLabAuthentication, selectedExperimentsParam);
		resp.getWriter().println(ExperimentJsonUploadProcessor.toJson(deleteExperiments(HiLabAuthentication,selectedExperimentsParam)));
   }
	
	/**
	 * Method to attempt to delete an experiment. Gives specific error message if something went wrong.
	 * @param email: the e-mail address of the entity having made the POST request
	 * @param selectedExperimentsParam: the Primary key of the experiment to delete
	 * @return a verification message for a deleted experiment or an error message for each part of the deletion that went wrong. 
	 */
	private List<Outcome> deleteExperiments(String email, String selectedExperimentsParam) {
	    ExperimentService expService = ExperimentServiceFactory.getExperimentService();
	    List<Long> experimentIds = ExperimentServletSelectedExperimentsFullLoadHandler.parseExperimentIds(selectedExperimentsParam);
	    if (experimentIds.isEmpty()) {
	      return createErrorOutcome("No experiment id specified for deletion");
	    }
	    Outcome outcome = new Outcome();
	    List<Outcome> outcomeList = Lists.newArrayList();
	    outcomeList.add(outcome);
	    try {
	      final Boolean deleteExperimentsResult = expService.deleteExperiments(experimentIds, email);
	      if (!deleteExperimentsResult) {
	        outcome.setError("Could not delete experiment. Rolled back.");
	      }
	    } catch (Exception e) {
	      outcome.setError("Could not delete experiment. Rolled back. Error: " + e.getMessage());
	    }

	    return outcomeList;
	  }
	
	/**
	 * Create a list of error messages
	 * @param msg: the parsed result
	 * @return: a list of separate messages containing context to the error
	 */
	public List<Outcome> createErrorOutcome(String msg) {
	    Outcome outcome = new Outcome(0, msg);
	    List<Outcome> outcomes = Lists.newArrayList(outcome);
	    return outcomes;
	  }
	
	/**
	 * Method to bypass strange behavior when referencing User profile
	 * When first attempting to delete, no matter if the credentials are correct. it will fail.
	 * However when the method is run again, directly in succession the delete will succeed if and only if the user credentials are correct
	 * Because of this unexplained behavior we (hack) this method so the request is always send twice
	 * If the user credentials are wrong it will still fail the second time
	 */
	public void bypass(String credentials, String experimentID){
		deleteExperiments(credentials,experimentID);
	}
}
